workspace "Abastecimento Integrado" "Solução de integração do aplicativo abastece-aí a automação da bomba no fluxo de abastecimento." {

    model {
        customer = person "Customer" "Cliente com conta no app abastece-aí."
        vip = person "VIP" "Atendente responsável pelo abastecimento."
        enterprise "Abastecimento Integrado" {
            
            abasteceai = softwareSystem "abastece-aí" "Permite ao cliente consultar produtos disponíveis, solicitar abastecimento com preset, acompanhar abastecimento e efetuar o pagamento."{
                mobileApp = container "App" "Aplicativo Mobile" "iOS, Android" "Mobile App"
                qrCode = container "QR Code API" "Gere o QR code" "Java, Spring Boot"
                wallet = container "Wallet" "Gerencia pagamentos" "Java, Spring Boot" "Existing System"
                realtime = container "Real-time Notifications" "API para notificações de eventos em tempo real" "AWS AppSync, GraphQL API"{
                    register = component "Register" "Registra um Tópico associado a um Canal" "GraphQL Mutation"
                    publish = component "Publish" "Envia eventos de notificação a um determinado Canal" "GraphQL Mutation"
                    subscribe = component "Subscribe" "Subscreve um Canal e recebe atualizações em tempo real" "GraphQL Subscription"
                }
            }
            backoffice = softwareSystem "Backoffice" "Gerencia dados dos parceiros" "Existing System"
            
            abastecimento = softwareSystem "Abastecimento" "Solução de abastecimento." "External System"{
                conecta = container "Gateway de integração Ipiranga."
                automacao = container "Comunica com a bomba de abastecimento."
            }
            payment = softwareSystem "Payment Partner" "Processa pagamento." "External System"
    
            customer -> vip "Informa que vai utilizar o abastece-aí ao"
            
            customer -> abasteceai "Consulta produtos, envia preset, acompanha evolução do abastecimento e efetua pagamento usando"
            vip -> abastecimento "Libera e realiza abastecimento usando"
            abasteceai -> abastecimento "Consulta produtos, envia preset, notifica pagamento usando"
            abastecimento -> abasteceai "Envia evolução do abastecimento para"
            abasteceai -> payment "Envia solicitação de pagamento usando"
            abasteceai -> backoffice "Valida dados de parceiros usando"
            
            customer -> mobileApp "Consulta produtos, envia preset, acompanha evolução do abastecimento e efetua pagamento usando"
            vip -> automacao "Usa"
            
            mobileApp -> qrCode "Valida QR code e consulta produtos em" "JSON/HTTPS"
            mobileApp -> subscribe "Subscreve canal FUEL em" "WSS/GraphQL:subscription"
            qrCode -> register "Registra o Tópico (cesta) no canal FUEL em" "HTTPS/GraphQL:mutation"
            qrCode -> conecta "Notifica atualização na cesta ao" "JSON/HTTPS"
            conecta -> automacao "Usa"
            automacao -> conecta "Usa"
            conecta -> qrCode "Cria/encerra cesta usando" "JSON/HTTPS"
            conecta -> publish "Envia evolução do abastecimento ao canal FUEL em" "HTTPS/GraphQL:mutation"
            qrCode -> wallet "Cria pedido em" "JSON/HTTPS"
            mobileApp -> wallet "Efetua pagamento em" "JSON/HTTPS"
            wallet -> payment "Processa pagamento em" "JSON/HTTPS"
            wallet -> qrCode "Notifica pagamento" "MQTT"
        }
 
    }

    views {
        dynamic * {
            customer -> abasteceai "Valida QR code e consulta produtos em"
            customer -> abasteceai "Seleciona produtos/valor e solicta abastecimento em"
            abasteceai -> abastecimento "Envia solicitação e preset para"
            customer -> vip "Sinaliza que vai utilizar o bastece-aí ao"
            vip -> abastecimento "Inicia abastecimento em"
            abastecimento -> abasteceai "Envia evolução de abastecimento para"
            vip -> abastecimento "Encerra abastecimento em"
            abastecimento -> abasteceai "Envia conclusão do abastecimento para"
            customer -> abasteceai "Efetua pagamento em"
            abasteceai -> abastecimento "Envia confirmação de pagamento para"
            autoLayout lr
        }
        
        systemlandscape "SystemLandscape" {
            include *
            autoLayout lr
        }
        
        systemContext abasteceai "SystemContext" {
            include *
            autoLayout lr
        }
        
        container abasteceai "Containers" {
            include *
            autoLayout lr
        }
        
        component realtime "Notifications" {
            include *
            autoLayout
        }

        styles {
            element "Software System" {
                background #1168bd
                color #ffffff
            }
            element "Person" {
                shape person
                background #08427b
                color #ffffff
            }
            element "External System" {
                background #999999
                color #ffffff
            }
            element "Existing System" {
                background #6c757d
                color #ffffff
            }
            element "Container" {
                background #438dd5
                color #ffffff
            }
            element "Web Browser" {
                shape WebBrowser
            }
            element "Mobile App" {
                shape MobileDeviceLandscape
            }
            
            element "Database" {
                shape cylinder
            }
        }
    }
        
}
